# PSC-Backup

Backup tool based on @cyanics work - written in C

## Build

Build using CMake.
Please note that OpenSSL 1.0.0 needs to be installed or at least available as source to do dynamic linking.

```
cmake . -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=arm-linux-gnueabihf-gcc -DOPENSSL_INCLUDE_DIR=<<OPENSSL INCLUDE DIR>> -DOPENSSL_LIBRARIES=<<OPENSSL LIB DIR>> -DCMAKE_DISABLE_TESTING=1
cmake --build .
```