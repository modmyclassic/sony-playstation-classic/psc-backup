#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include "signer.h"

#include "zip.h"


typedef enum Backup_Tag
{
    BACKUP_NORMAL   = 0U,
    BACKUP_RECOVERY = 1U,
    BACKUP_KERNEL   = 2U,
    BACKUP_BOTH     = 3U,
    BACKUP_INVALID  = 4U,
    BACKUP_TEST     = 5U
} Backup_e;

typedef struct Input_File_Tag
{
    char* filepath;
    char* entryname;
} Input_File_t;


static void get_backup_type(char* argv, Backup_e* const backup_type);
static void get_output_dir(char *argv, char** dir);
static void create_zip(char* path, Input_File_t* files, uint8_t file_cnt);



static void get_output_dir(char *argv, char** dir)
{
    DIR* directory = opendir(argv);
    if (directory)
    {
        *dir = argv;
        /* Directory exists. */
        closedir(directory);
    }
    else
    {
        *dir = NULL;
    }
}

static void get_backup_type(char* argv, Backup_e* const backup_type)
{
    if (!strcmp(argv, "normal"))
    {
        *backup_type = BACKUP_NORMAL;
    }
    else if (!strcmp(argv, "recovery"))
    {
        *backup_type = BACKUP_RECOVERY;
    }
    else if (!strcmp(argv, "all"))
    {
        *backup_type = BACKUP_BOTH;
    }
    else if (!strcmp(argv, "test"))
    {
        *backup_type = BACKUP_TEST;
    }
    else if (!strcmp(argv, "kernel"))
    {
        *backup_type = BACKUP_KERNEL;
    }
    else
    {
        *backup_type = BACKUP_INVALID;
    }
}

static void create_zip(char* path, Input_File_t* files, uint8_t file_cnt)
{
    printf("Creating ZIP at %s...", path);
    struct zip_t *zip = zip_open(path, ZIP_DEFAULT_COMPRESSION_LEVEL, 'w');
    {
        for (uint8_t i = 0U; i < file_cnt; i++)
        {
            zip_entry_open(zip, files[i].entryname);
            {
                zip_entry_fwrite(zip, files[i].filepath);
            }
            zip_entry_close(zip);
        }
    }
    zip_close(zip);
    printf("Done!\n");
}



int main(int argc, char **argv)
{
    Backup_e backup_type = BACKUP_INVALID;
    char* output_dir = NULL;

    char pem_path[1024] = "";
    char zip_path[1024] = "";

    setbuf(stdout, NULL);

    if (argc == 3)
    {
        get_backup_type(argv[1], &backup_type);
        get_output_dir(argv[2], &output_dir);
    }
    else
    {
        printf("Syntax ERROR: Please use backup_psc [TYPE] [OUTPUT_DIR]\n");
    }

    if ((backup_type != BACKUP_INVALID)
            && (output_dir != NULL))
    {
        if (BACKUP_NORMAL == backup_type)
        {
            Input_File_t files[4U];
            uint8_t file_cnt = 4U;

            memset(pem_path, 0x0, 1024U);
            memset(zip_path, 0x0, 1024U);

            strcpy(pem_path, output_dir);
            strcat(pem_path, "/normal_pubk.pem");
            strcpy(zip_path, output_dir);
            strcat(zip_path, "/LBOOT.EPB");

            signer_init(pem_path);

            printf("Creating NORMAL Backup\n");

            files[0U] = (Input_File_t){"/dev/disk/by-partlabel/BOOTIMG1", "boot.img"};
            files[1U] = (Input_File_t){"/dev/disk/by-partlabel/ROOTFS1", "rootfs.ext4"};
            files[2U] = (Input_File_t){"/dev/disk/by-partlabel/USRDATA", "userdata.ext4"};
            files[3U] = (Input_File_t){"/dev/disk/by-partlabel/TEE1", "tz.img"};

            create_zip(zip_path, files, file_cnt);
            signer_signfile(zip_path);

            signer_close();
        }
        if (BACKUP_RECOVERY == backup_type)
        {
            Input_File_t files[3U];
            uint8_t file_cnt = 3U;

            memset(pem_path, 0x0, 1024U);
            memset(zip_path, 0x0, 1024U);

            strcpy(pem_path, output_dir);
            strcat(pem_path, "/recovery_pubk.pem");
            strcpy(zip_path, output_dir);
            strcat(zip_path, "/LRECOVERY.EPB");

            signer_init(pem_path);

            printf("Creating RECOVERY Backup\n");

            files[0U] = (Input_File_t){"/dev/disk/by-partlabel/BOOTIMG2", "boot.img"};
            files[1U] = (Input_File_t){"/dev/disk/by-partlabel/ROOTFS2", "rootfs.ext4"};
            files[2U] = (Input_File_t){"/dev/disk/by-partlabel/TEE2", "tz.img"};

            create_zip(zip_path, files, file_cnt);
            signer_signfile(zip_path);

            signer_close();
        }
        if (BACKUP_KERNEL == backup_type)
        {
            Input_File_t files[1U];
            uint8_t file_cnt = 1U;

            memset(pem_path, 0x0, 1024U);
            memset(zip_path, 0x0, 1024U);

            strcpy(pem_path, output_dir);
            strcat(pem_path, "/kernel_pubk.pem");
            strcpy(zip_path, output_dir);
            strcat(zip_path, "/LBOOT_KERNEL.EPB");

            signer_init(pem_path);

            printf("Creating KERNEL Backup\n");

            files[0U] = (Input_File_t){"/dev/disk/by-partlabel/BOOTIMG1", "boot.img"};

            create_zip(zip_path, files, file_cnt);
            signer_signfile(zip_path);

            signer_close();
        }
    }

    return 0;
}
