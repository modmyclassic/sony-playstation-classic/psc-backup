#ifndef SIGNER_H
#define SIGNER_H

#include <stdint.h>
#include <string.h>
#include <stdlib.h>


#ifdef __clang__
/* Swap bytes in 64-bit value.  */
#define __bswap_constant_64(x)			\
  ((((x) & 0xff00000000000000ull) >> 56)	\
   | (((x) & 0x00ff000000000000ull) >> 40)	\
   | (((x) & 0x0000ff0000000000ull) >> 24)	\
   | (((x) & 0x000000ff00000000ull) >> 8)	\
   | (((x) & 0x00000000ff000000ull) << 8)	\
   | (((x) & 0x0000000000ff0000ull) << 24)	\
   | (((x) & 0x000000000000ff00ull) << 40)	\
   | (((x) & 0x00000000000000ffull) << 56))
#endif

void signer_init(char* pem_path);

void signer_signfile(char* filename);

void signer_close();



#endif // SIGNER_H
