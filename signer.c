#include "signer.h"
#include <time.h>
#include <openssl/sha.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <stdlib.h>

#define SIG_PARAM_SIZE          ( 0x3e )
#define RAW_SIGNATURE_SIZE      ( 0xfc2 )
#define HASH_SIZE               ( 0x20 )
#define SIGNED_AREA_SIZE        ( 0x200 )
#define COMMENT_SIZE            ( 0x20 )
#define PRE_PADDING_SIZE        ( 0x10 )
#define POST_PADDING_SIZE       ( 0x1d0 )
#define PADDING_SIZE            ( 0xdc2 )

#define BYTE unsigned char


typedef struct RawC_Tag
{
    union
    {
        BYTE bytes[SIGNED_AREA_SIZE];
        struct {
            BYTE pre_padding[PRE_PADDING_SIZE];
            uint8_t hash[HASH_SIZE];
            BYTE post_padding[POST_PADDING_SIZE];
        } content;
    } signed_area;
    BYTE padding[PADDING_SIZE];
} RawC_t;

typedef struct SigParams_Tag
{
    BYTE rawc[0x4];
    uint16_t version;
    uint64_t chunk_size;
    uint64_t chunk_gap;
    uint64_t chunk_count;
    BYTE comment[COMMENT_SIZE];
} SigParams_t;



static RSA* rsa = NULL;

static void rsacreatekeys(const char* const pubkey_path);
static void rsaencrypt(BYTE signed_data[SIGNED_AREA_SIZE]);
static void randomBytes(BYTE* bytes, uint32_t noOfBytes);
static void init(RawC_t* rawc, SigParams_t* params);
static void create_hash(FILE* file, SigParams_t* const params, BYTE raw_sig_params[SIG_PARAM_SIZE], BYTE hash[HASH_SIZE]);
static void getParameters(FILE* file, SigParams_t* const params);
static void getRawSignature(const RawC_t* const signature, BYTE raw_signature[RAW_SIGNATURE_SIZE]);


static void randomBytes(BYTE* bytes, uint32_t noOfBytes)
{
    for (uint32_t i = 0U; i < noOfBytes; i++)
    {
        bytes[i] = (unsigned char) (rand() % 256);
    }
}

static void init(RawC_t* rawc, SigParams_t* params)
{
    memset(rawc, 0, sizeof (*rawc));
    memset(params, 0, sizeof (*params));

    params->rawc[0] = 'R';
    params->rawc[1] = 'A';
    params->rawc[2] = 'W';
    params->rawc[3] = 'C';
    params->version = 1;

    time_t t;
    srand((unsigned) time(&t));
    /* Disabled because its causing issues right now. Further investigation needs to be done why.
    randomBytes(rawc->signed_area.content.pre_padding, PRE_PADDING_SIZE);
    randomBytes(rawc->signed_area.content.post_padding, POST_PADDING_SIZE);
    */
    randomBytes(rawc->padding, PADDING_SIZE);
    randomBytes(params->comment, COMMENT_SIZE);
}

static void create_hash(FILE* file, SigParams_t* const params, BYTE raw_sig_params[SIG_PARAM_SIZE], BYTE hash[HASH_SIZE])
{
    printf("Hashing...");
    BYTE* chunk_buffer;
    uint64_t read_size;

    /* Allocate Buffer for chunks */
    chunk_buffer = malloc((size_t)params->chunk_size);

    /* Initialize SHA256 context for hashing*/
    SHA256_CTX context;
    SHA256_Init(&context);

    /* Set read pointer in file to begin of the file */
    fseek(file, 0, SEEK_SET);

    /* Read each chunk */
    for (uint64_t file_idx = 0U ; file_idx < (params->chunk_count); file_idx++)
    {
        /* Read Chunk into chunk_buffer */
        read_size = fread(chunk_buffer, 1, (unsigned)params->chunk_size, file);
        /* Update Hash */
        SHA256_Update(&context, chunk_buffer, (size_t)read_size);
        /* Jump to next chunk */
        fseek(file, (uint32_t)params->chunk_gap, SEEK_CUR);
    }

    /* Finally add signature parameters */
    SHA256_Update(&context, raw_sig_params, SIG_PARAM_SIZE);
    SHA256_Final(hash, &context);

    free(chunk_buffer);

    printf("Done!\n");

}

static void getParameters(FILE* file, SigParams_t* const params)
{
    uint64_t filesize;
    fseek(file, 0, SEEK_END);
    filesize = (uint64_t)ftell(file);
    fseek(file, 0, SEEK_SET);

    params->chunk_gap = 0;
    if (0 == params->chunk_size)
    {
        params->chunk_size = (uint64_t) filesize;
    }
    params->chunk_count = (filesize + params->chunk_gap) / (params->chunk_size + params->chunk_gap);
}


static void getRawSignature(const RawC_t* const signature, BYTE raw_signature[RAW_SIGNATURE_SIZE])
{
    memcpy(&(raw_signature[0x0]), &(signature->signed_area.bytes), SIGNED_AREA_SIZE);
    memcpy(&(raw_signature[0x200]), &(signature->padding), PADDING_SIZE);
}

static void getRawSignatureParams(const SigParams_t* const params, BYTE raw_params[SIG_PARAM_SIZE])
{
    uint64_t swapped_chunk_size;
    uint64_t swapped_chunk_gap;
    uint64_t swapped_chunk_count;

    swapped_chunk_size = __bswap_constant_64(params->chunk_size);
    swapped_chunk_gap = __bswap_constant_64(params->chunk_gap);
    swapped_chunk_count = __bswap_constant_64(params->chunk_count);
    memcpy(&(raw_params[0x0]), params->rawc, 0x4);
    memcpy(&(raw_params[0x4]), &(params->version), sizeof (params->version));
    memcpy(&(raw_params[0x6]), &swapped_chunk_size, sizeof(swapped_chunk_size));
    memcpy(&(raw_params[0xe]), &swapped_chunk_gap, sizeof(swapped_chunk_gap));
    memcpy(&(raw_params[0x16]), &swapped_chunk_count, sizeof (swapped_chunk_count));
    memcpy(&(raw_params[0x1e]), &(params->comment), COMMENT_SIZE);
}

static void rsaencrypt(BYTE signed_data[SIGNED_AREA_SIZE])
{
    printf("Encrypting...");
    BYTE enc_data[SIGNED_AREA_SIZE] = "";
    RSA_private_encrypt(SIGNED_AREA_SIZE, signed_data, enc_data, rsa, RSA_NO_PADDING);
    memcpy(signed_data, enc_data, SIGNED_AREA_SIZE);
    printf("Done!\n");
}

static void rsacreatekeys(const char* const pubkey_path)
{
    BIGNUM *bne = NULL;
    BIO *bp_public = NULL;

    int bits = 4096;
    unsigned long e = RSA_F4;

    printf("Creating RSA keys...");

    bne = BN_new();
    BN_set_word(bne,e);

    RSA_generate_key_ex(rsa, bits, bne, NULL);

    bp_public = BIO_new_file(pubkey_path, "w+");

    PEM_write_bio_RSA_PUBKEY(bp_public, rsa);
    BIO_free_all(bp_public);
    BN_free(bne);

    printf("Done!\n");
}

void signer_init(char* pem_path)
{
    rsa = RSA_new();
    rsacreatekeys(pem_path);
}


void signer_signfile(char* filename)
{
    RawC_t signature;
    SigParams_t signature_params;
    BYTE signature_raw[RAW_SIGNATURE_SIZE] = "";
    BYTE signatureparams_raw[SIG_PARAM_SIZE] = "";
    FILE *file = fopen(filename, "rb");;

    init(&signature, &signature_params);

    getParameters(file, &signature_params);

    getRawSignatureParams(&signature_params, signatureparams_raw);

    create_hash(file, &signature_params, signatureparams_raw, signature.signed_area.content.hash);

    rsaencrypt(signature.signed_area.bytes);

    getRawSignature(&signature, signature_raw);

    freopen(filename, "ab", file);

    fwrite(signatureparams_raw, 1, SIG_PARAM_SIZE, file);

    fwrite(signature_raw, 1, RAW_SIGNATURE_SIZE, file);

    fclose(file);
}

void signer_close()
{
    RSA_free(rsa);
}
